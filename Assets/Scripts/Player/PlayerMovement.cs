﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public int speedMovement;
    public float jumpVelocity;
    public Rigidbody2D rigidbody2DPlayer;
    private bool isJumping = false;
    
    void Start()
    {

    }

    void Update()
    {
        bool left = Input.GetKey(KeyCode.Q);
        bool right = Input.GetKey(KeyCode.D);
        bool jump = Input.GetKey(KeyCode.Space);
        bool sprint = Input.GetKey(KeyCode.LeftShift);
        bool crouch = Input.GetKey(KeyCode.LeftControl);

        if (right)
        {
            transform.Translate(Vector3.right * speedMovement * Time.deltaTime);
        }
        if (left)
        {
            transform.Translate(Vector3.left * speedMovement * Time.deltaTime);
        }
        if (jump && !isJumping)
        {
            isJumping = true;
            rigidbody2DPlayer.velocity = Vector2.up * jumpVelocity;
        }
        if (sprint)
        {
            //Futur fonction sprint
        }
        if (crouch)
        {
            //Futur fonction crouch
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        isJumping = false;
        //Next Step : change for detecting walls
    }
}
