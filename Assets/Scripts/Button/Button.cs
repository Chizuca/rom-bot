﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonMenu : MonoBehaviour
{
    public GameObject playButton , optionMenu;    //les boutons play et le menu
    void Start()
    {
       
    }
    void Update()
    {
        
    }
    public void QuitGame()    //Bouton Quit pour fermer le jeu
    {
        Debug.Log("Quitting game...");
        Application.Quit();
    }
    public void SceneLoader(int SceneIndex)  //Bouton pour se balader de niveau en niveau
    {
        SceneManager.LoadScene(SceneIndex);
    }
    public void Option()       //Bouton pour afficher le menu et aussi caché les boutons de départ
    {
        playButton.SetActive(false);
        optionMenu.SetActive(true);
    }
}
