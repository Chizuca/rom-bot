﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour
{
    public int speedCloud; //rapidité du nuage
    private Vector2 screenBounds;
    public GameObject cloud;
    private float objectHeight;
    public int respawnTime;
    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        StartCoroutine(EnemyWave());
        objectHeight = cloud.transform.GetComponent<SpriteRenderer>().bounds.extents.y;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.left * Time.deltaTime * speedCloud);
        if (transform.position.x <= -40)
        {
            Vector3 pos = transform.position;
            pos.x = 470;
            transform.position = pos;
        }
    }
    private void spawnEnemy()
    {
        GameObject a = Instantiate(cloud) as GameObject;
        a.transform.position = new Vector2(screenBounds.x, Random.Range(-screenBounds.y + objectHeight, screenBounds.y - objectHeight));

    }
    IEnumerator EnemyWave()
    {
        while (true)
        {
            yield return new WaitForSeconds(respawnTime);
            spawnEnemy();

        }

    }
}
